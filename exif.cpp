#include <exiv2/exiv2.hpp>
#include <sstream>
#include <iomanip>
#include <ctime>

#include "exif.h"
#include "entry.h"
#include "album.h"
#include "debug.h"

static std::map<int, std::string> ExifMetering = {
	{ 0, "unknown" },
	{ 1, "average" },
	{ 2, "center weighted" },
	{ 3, "spot" },
	{ 4, "multi-spot" },
	{ 5, "multi-segment" },
	{ 6, "partial" },
	{ 255, "other" }
};

static std::map<int, std::string> ExifMeteringCanon = {
	{ 0, "default" },
	{ 1, "spot" },
	{ 3, "evaluative" },
	{ 4, "partial" },
	{ 5, "center-weighted" }
};

static std::map<int, std::string> ExifExpoProg = {
	{ 0, "not defined" },
	{ 1, "manual" },
	{ 2, "normal" },
	{ 3, "aperture priority" },
	{ 4, "shutter priority" },
	{ 5, "creative" },
	{ 6, "action" },
	{ 7, "portrait" },
	{ 8, "landscape" }
};

static std::map<int, std::string> ExifExpoProgCanon = {
	{ 0, "auto/creative" },
	{ 1, "program" },
	{ 2, "Tv priority" },
	{ 3, "Av priority" },
	{ 4, "manual" },
	{ 5, "A-DEP" }
};

void Exif_data::handle_exiv_warning(int lvl, const char* msg) {
	std::string msg_chomped = msg;
	while (msg_chomped.back() == '\n')
		msg_chomped.pop_back();

	Debug(3) << "Exiv2 message: " << msg_chomped << " (level " << lvl << ").";
}

void Exif_data::init() {
	Exiv2::XmpParser::initialize();
	Exiv2::LogMsg::setHandler(handle_exiv_warning);

// Needed in current Exiv2 version to support HEIF and AVIF
#ifdef EXV_ENABLE_BMFF
	Exiv2::enableBMFF(1);
#endif

}

int Exif_data::read_key(const std::string& key, std::string* res) {
	if (exifdata->findKey(Exiv2::ExifKey(key)) != exifdata->end()) {
		*res = (*exifdata)[key].toString();
		return 1;
	}
	return 0;
}

int Exif_data::read_key(const std::string& key, float* res) {
	if (exifdata->findKey(Exiv2::ExifKey(key)) != exifdata->end()) {
		*res = (*exifdata)[key].toFloat();
		return 1;
	}
	return 0;
}

int Exif_data::read_key(const std::string& key, int64_t* res) {
	if (exifdata->findKey(Exiv2::ExifKey(key)) != exifdata->end()) {
		*res = (*exifdata)[key].toInt64();
		return 1;
	}
	return 0;
}

int Exif_data::read_key_xmp(const std::string& key, std::string* res) {
	if (xmpdata->findKey(Exiv2::XmpKey(key)) != xmpdata->end()) {
		*res = (*xmpdata)[key].toString();
		return 1;
	}
	return 0;
}

void Exif_data::read(const Image* ptr) {
	Exiv2::Image::UniquePtr image;

	image_ptr = ptr;

	try {
		image = Exiv2::ImageFactory::open(std::string(ptr->parent->gallery_root / ptr->directory / ptr->filename));

		if (!image.get()) return;

		image->readMetadata();
	}
	catch (Exiv2::Error& e) {
		Warn() << "Caught Exiv2 exception '" << e.what() << "' while reading " << ptr->filename;
		return;
	}

	exifdata = &image->exifData();
	xmpdata = &image->xmpData();

	read_key_xmp("Xmp.dc.subject", &raw_data.subject);
	update_keywords();

	if (exifdata->empty()) {
		Debug(3) << "No exif data for: " << ptr->filename;
		return;
	}

	Debug(3) << "Reading exif data for: " << ptr->filename;
	read_key("Exif.Image.Model", &raw_data.model);
	if (!read_key("Exif.Photo.DateTimeOriginal", &raw_data.date))
		read_key("Exif.Image.DateTimeOriginal", &raw_data.date);
	if (!read_key("Exif.Photo.ExposureTime", &raw_data.shutter))
		read_key("Exif.Photo.ShutterSpeedValue", &raw_data.shutter);
	if (!read_key("Exif.Photo.FNumber", &raw_data.aperture))
		read_key("Exif.Photo.ApertureValue", &raw_data.aperture);
	read_key("Exif.Photo.ISOSpeedRatings", &raw_data.iso);
	read_key("Exif.Photo.FocalLength", &raw_data.focal);
	read_key("Exif.Photo.ExposureBiasValue", &raw_data.exp_comp);

	// read short/long focal length (measured in "focal units")
	if (exifdata->findKey(Exiv2::ExifKey("Exif.CanonCs.Lens")) != exifdata->end()) {
		raw_data.long_foc = (*exifdata)["Exif.CanonCs.Lens"].toInt64(0) * (*exifdata)["Exif.CanonCs.Lens"].toInt64(2);
		raw_data.short_foc = (*exifdata)["Exif.CanonCs.Lens"].toInt64(1) * (*exifdata)["Exif.CanonCs.Lens"].toInt64(2);
	}

	if (!read_key("Exif.Photo.LensModel", &raw_data.lens_name))
		read_key("Exif.Canon.LensModel", &raw_data.lens_name);

	long i;

	if (read_key("Exif.CanonCs.MeteringMode", &i))
		raw_data.metering = ExifMeteringCanon[i];
	else if (read_key("Exif.Photo.MeteringMode", &i))
		raw_data.metering = ExifMetering[i];

	if (read_key("Exif.CanonCs.FlashMode", &i))
		raw_data.flash = (i > 0);
	else if (read_key("Exif.Photo.Flash", &i))
		raw_data.flash = i & 1;

	if (read_key("Exif.CanonCs.ExposureProgram", &i))
		raw_data.expo_prog = ExifExpoProgCanon[i];
	else if (read_key("Exif.Photo.ExposureMode", &i) || read_key("Exif.Photo.ExposureProgram", &i))
		raw_data.expo_prog = ExifExpoProg[i];

	generate_strings();
	show_data();
}

void Exif_data::read(const Image* ptr, const exif_db_data& data) {
	image_ptr = ptr;

	raw_data = data;
	generate_strings();
	update_keywords();
}

void Exif_data::generate_strings() {
	// generate formatted_date
	if (!raw_data.date.empty() && Exiv2::exifTime(raw_data.date.c_str(), &date_tm) == 0) {
		char buf[80];
		if (strftime(buf, 80, image_ptr->parent->settings->date_format.c_str(), &date_tm))
			formatted_date = std::string(static_cast<char*>(buf));
	}

	// generate formatted_exif
	if (!raw_data.model.empty()) {
		formatted_exif = raw_data.model;

		if (raw_data.short_foc && raw_data.long_foc && image_ptr->parent->settings->lenses.count(Lens(raw_data.short_foc, raw_data.long_foc)))
			formatted_exif += ", " + image_ptr->parent->settings->lenses[Lens(raw_data.short_foc, raw_data.long_foc)];

		formatted_exif += "<br>\n";

		if (raw_data.aperture && raw_data.iso && !raw_data.shutter.empty()) {
			std::ostringstream temp;
			temp << raw_data.aperture;
			std::string ap = temp.str();
			formatted_exif += raw_data.shutter + "s, f/" + ap + ", ISO " + std::to_string(raw_data.iso);
		}

		if (raw_data.exp_comp) {
			std::ostringstream temp;
			temp << raw_data.exp_comp;
			std::string ex = temp.str();
			formatted_exif += (raw_data.exp_comp > 0 ? ", +" : ", ") + ex + "EV";
		}

		if (raw_data.focal) {
			std::ostringstream temp;
			temp << raw_data.focal;
			std::string foc = temp.str();
			formatted_exif += ", " + foc + "mm";
		}

		formatted_exif += "<br>\n";

		if (!raw_data.metering.empty() && !raw_data.expo_prog.empty())
			formatted_exif += raw_data.expo_prog + ", " + raw_data.metering + " metering" + (raw_data.flash ? ", flash" : "");
	}
}

void Exif_data::update_keywords() {
#pragma omp critical(strtok)
	if (!raw_data.subject.empty()) {
		char* raw;
		raw = strdup(raw_data.subject.c_str());
		char* token = std::strtok(raw, ", ");
		while (token != NULL) {
			keywords.push_back(token);
			token = std::strtok(NULL, ", ");
		}
		free(raw);
	}
}

exif_db_data Exif_data::get_rawdata() const {
	return raw_data;
}

void Exif_data::show_data() {
	Debug(3) << "Exif model: " << raw_data.model;
	Debug(3) << "Exif date: " << raw_data.date;
	Debug(3) << "Exif shutter: " << raw_data.shutter;
	Debug(3) << "Exif aperture: " << raw_data.aperture;
	Debug(3) << "Exif exposure compensation: " << raw_data.exp_comp;
	Debug(3) << "Exif ISO: " << raw_data.iso;
	Debug(3) << "Exif flash: " << raw_data.flash;
	Debug(3) << "Exif focal: " << raw_data.focal;
	Debug(3) << "Exif lens name: " << raw_data.lens_name;
	Debug(3) << "Exif short focal: " << raw_data.short_foc;
	Debug(3) << "Exif long focal: " << raw_data.long_foc;
	Debug(3) << "Exif metering: " << raw_data.metering;
	Debug(3) << "Exif exposure program: " << raw_data.expo_prog;

	std::vector<std::string> tags = {
		"Exif.Image.DateTime",
		"Exif.Image.ExposureTime",
		"Exif.Image.FNumber",
		"Exif.Image.ExposureProgram",
		"Exif.Image.ISOSpeedRatings",
		"Exif.Image.DateTimeOriginal",
		"Exif.Image.ShutterSpeedValue",
		"Exif.Image.ApertureValue",
		"Exif.Image.ExposureBiasValue",
		"Exif.Image.MeteringMode",
		"Exif.Image.Flash",
		"Exif.Image.FocalLength",
		"Exif.Photo.ExposureTime",
		"Exif.Photo.FNumber",
		"Exif.Photo.ExposureProgram",
		"Exif.Photo.ISOSpeedRatings",
		"Exif.Photo.ISOSpeed",
		"Exif.Photo.DateTimeOriginal",
		"Exif.Photo.ShutterSpeedValue",
		"Exif.Photo.ApertureValue",
		"Exif.Photo.ExposureBiasValue",
		"Exif.Photo.MeteringMode",
		"Exif.Photo.Flash",
		"Exif.Photo.FocalLength",
		"Exif.Photo.ExposureMode",
		"Exif.Photo.LensSpecification",
		"Exif.Photo.LensMake",
		"Exif.Photo.LensModel",
		"Exif.Canon.FocalLength",
		"Exif.Canon.LensModel",
		"Exif.CanonCs.FlashMode",
		"Exif.CanonCs.DriveMode",
		"Exif.CanonCs.FocusMode",
		"Exif.CanonCs.ISOSpeed",
		"Exif.CanonCs.MeteringMode",
		"Exif.CanonCs.ExposureProgram",
		"Exif.CanonCs.FocusType",
		"Exif.CanonCs.LensType",
		"Exif.CanonCs.Lens",
		"Exif.CanonCs.ShortFocal",
		"Exif.CanonSi.ISOSpeed"
	};

	std::string s;
	for (auto i : tags) {
		if (read_key(i, &s)) {
			Debug(4) << std::setw(40) << i << ": " << s;
		} else {
			Debug(4) << std::setw(40) << i << ": --- ";
		}
	}
}
