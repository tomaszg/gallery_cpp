#pragma once

#include <filesystem>
#include <set>
#include <sstream>

#include "settings.h"

class HtmlOutput : public std::stringstream {
private:
	std::filesystem::path output_file;

public:
	explicit HtmlOutput(std::filesystem::path ofile) : output_file(ofile){};

	void update_file(); // compares istr with contents of ofile and replaces it if necessary
};

void createdir(std::filesystem::path filename);
void copyfile(std::filesystem::path ifile, std::filesystem::path ofile);

bool compare_file_timestamp(std::filesystem::path ifile, std::filesystem::path ofile); // returns 1 if ofile exists and has the same size and mtime as ifile

std::set<std::filesystem::path> find_files(std::filesystem::path dir);

std::string htmlencode(const std::string& data);
