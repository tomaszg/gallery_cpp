TODO:

porting features:
* escape album names (see branch "escape")
* rss file generation
* respect DATE: value
* css cascade

fixes and checks:
* exif: check data string
* exif debug: include file name in exiv2 warning message
* debug: review levels and messages logic and consistency
* debug: see if it is possible to make messages work well with parallel processing
* error handling: better check for errors e.g. related to disk operations
* cache: consistency with Perl version: relative or absolute path?
* cache: locale dependency - decimal digit
* cache: album thumbs get in a child directory, as they are created in album class
* cache: include timestamp also for album thumbs
* review support of other file formats, thumbs for foo.jpg and foo.png would coincide
* check if thumb_empty is needed
* possibly clean-up heavy includes from headers (regex, exiv)
* iso -> utf
* consider fallback jpg for webp files
* force table cells width
* use of static variables in Album class precludes from having 2 separates gallery trees in one instance
* make -c work in dry-run mode

new features:
* use some database for cache
* cache Exif data
* make exif text configurable, possibly borrow Geeqie overlay format code
* orphaned files delete
