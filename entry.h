#pragma once

#include <string>
#include <filesystem>

#include "settings.h"
#include "exif.h"

class Album;

class Entry {
public:
	std::filesystem::path directory; // location of the entry, relative to gallery root
	std::filesystem::path filename; // filename of the entry, where applicable
	std::string title; // title of the entry
	std::string date; // date of the entry, where applicable
	const Album* parent;

	virtual ~Entry() {}

	virtual std::string highlight() const;
	virtual std::string generate(const std::string& where = "", int cols = 0, int* cur_col = nullptr) = 0;

protected:
	static std::string colspan(int n);

private:
	virtual int create_thumb();
	virtual void generate_html();
};

class Image : public Entry {
public:
	Image(Album* parent_ptr, std::string file, int num);

	std::string highlight() const override;
	int create_thumb() override;
	int create_image();
	std::string generate(const std::string& where = "", int cols = 0, int* cur_col = 0) override;
	void read_exif();

private:
	Exif_data exif;

	std::filesystem::path thumb_location;
	std::filesystem::path image_location;
	std::filesystem::path html_name;
	int number;
	// note that in C++17 clock and epoch are implementation dependent. GCC source code specifies epoch to be 2174-01-01 00:00:00 UTC, see libstdc++-v3/include/bits/fs_fwd.h.
	// timestamp in ticks since epoch with 1s resolution
	long long int timestamp;

	void generate_html() override;
};

class Break : public Entry {
public:
	Break(Album* parent_ptr, const std::string& txt);

	std::string generate(const std::string& where = "", int cols = 0, int* cur_col = 0) override;
};

class Break_start : public Entry {
public:
	Break_start(Album* parent_ptr, const std::string& txt);

	std::string generate(const std::string& where = "", int cols = 0, int* cur_col = 0) override;
};

class Break_end : public Entry {
public:
	Break_end(Album* parent_ptr);

	std::string generate(const std::string& where = "", int cols = 0, int* cur_col = 0) override;
};

class Link : public Entry {
public:
	Link(Album* parent_ptr, const std::string& name, std::string url);

	std::string highlight() const override;
	std::string generate(const std::string& where = "", int cols = 0, int* cur_col = 0) override;

private:
	std::string url;
	std::string thumb;
};
