<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset={{ charset }}">
  <meta name="revisit-after" content="2 days">
  <meta name="robots" content="all">
  <meta name="Keywords" content="{{ keywords }}">
{{ style }}
{% block rss %}
  <link rel="alternate" type="application/rss+xml" title="RSS" href="{{ rss }}">
{% endblock %}
{% block contents_rel %}
  <link rel="Contents" href="{{ url }}">
{% endblock %}
{% block next_rel %}
  <link rel="Next" href="{{ url }}">
{% endblock %}
{% block prev_rel %}
  <link rel="Prev" href="{{ url }}">
{% endblock %}
  <title>{{ title }}</title>
{% block script %}
  <script type="text/javascript">
    <!--
      function getKey(event) {
        if (!event) event = window.event;
        if (event.keyCode) code = event.keyCode;
        else if (event.which) code = event.which;
        if (event.shiftKey) {
{% block prev %}
        if (code == 37) {
            document.location = '{{ url }}';
          }
{% endblock %}
{% block up %}
          if (code == 38) {
            document.location = '{{ url }}';
          }
{% endblock %}
{% block next %}
          if (code == 39) {
            document.location = '{{ url }}';
          }
{% endblock %}
        }
        return true;
      }
      document.onkeypress = getKey;
    // -->
  </script>
{% endblock %}
 </head>
 <body>
{{ header }}
  <table cellspacing="0">
   <tr>
    <td {{ cs }} class="title">{{ title }}</td>
{% block date %}
    <td class="date">{{ date }}</td>
{% endblock %}
   </tr>
   <tr>
    <td {{ cs2 }} class="parent_links">
{{ parent }}
    </td>
    <td class="nav_links">
{% block about %}     <a href=\"about.html\">About</a>{% endblock %}
{% block stats %}     <a href="/cgi-bin/stats2.cgi?{{ filename }}">Most Viewed&nbsp;&nbsp;</a>{% endblock %}
{% block prev %}<a href="{{ url }}">{{ prev }}</a>{% endblock %}{% block prev_dis %}{{ prev }}{% endblock %}
&nbsp;&nbsp;
{% block up %}<a href="{{ url }}">{{ up }}</a>{% endblock %}{% block up_dis %}{{ up }}{% endblock %}
&nbsp;&nbsp;
{% block next %}<a href="{{ url }}">{{ next }}</a>{% endblock %}{% block next_dis %}{{ next }}{% endblock %}
    </td>
    </tr>
{% block more %}

   <tr><td colspan="2"></td>
    <td class="more_link">
    <span class="more_link"><a href="{{ more_link }}"  target="_blank">Wi?cej zdj?? {{ more_name }}</a></span>
    </td>
   </tr>
{% endblock %}
