{% include simple-template/header.tpl %}
   <tr>
    <td class="image" colspan="2">
{% block next_img %}
     <a href="{{ url }}">
{% endblock %}
     <img alt="{{ alt }}" class="image" src="{{ src }}">
{% block next_img2 %}
     </a>
{% endblock %}
    </td>
   </tr>
{% block text %}
    <tr><td colspan="2" align=center class="break">
    {{ text }}
    </td></tr>
{% endblock %}
{% block exif %}
   <tr>
    <td class="exif" colspan="2"><img src="{{ exif_url }}" alt="exif"><br>
     {{ exif }}
    </td>
   </tr>
{% endblock %}
{% include simple-template/footer.tpl %}
