#define _LIBCPP_ENABLE_CXX17_REMOVED_AUTO_PTR

#include "settings.h"

std::ostream& operator<<(std::ostream& str, const ImageSize& i) {
	str << i.width << 'x' << i.height;
	return str;
}

Lens::Lens(int foc1, int foc2) : focal_short(foc1), focal_long(foc2) {}

Band::Band(const std::string& n, const std::string& link) : name(n), url(link) {}

std::ostream& operator<<(std::ostream& str, const Lens& l) {
	str << l.focal_short << '-' << l.focal_long;
	return str;
}

Unsharp::Unsharp(double rad, double sig, double am, double thres) : radius(rad), sigma(sig), amount(am), threshold(thres) {}

std::ostream& operator<<(std::ostream& str, const Unsharp& u) {
	str << u.radius << 'x' << u.sigma << '+' << u.amount << '+' << u.threshold;
	return str;
}

Settings Settings::inherit() const {
	Settings set = *this;
	set.local_album_size.clear();
	set.local_image_size.clear();
	set.local_thumb_size.clear();
	set.local_columns = 0;
	set.local_css_file.clear();
	set.local_meta_keywords.clear();
	set.options.leaf = 0;
	set.options.hidden = 0;
	set.options.count = 0;
	set.options.count_dir = 0;
	set.highlight.clear();

	return set;
}

bool operator==(const ImageSize& lhs, const ImageSize& rhs) {
	return (lhs.width == rhs.width) && (lhs.height == rhs.height);
}

bool operator==(const Unsharp& lhs, const Unsharp& rhs) {
	return (lhs.radius == rhs.radius) && (lhs.sigma == rhs.sigma) && (lhs.amount == rhs.amount) && (lhs.threshold == rhs.threshold);
}
