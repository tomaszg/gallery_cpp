#pragma once

#include "settings.h"

#ifdef graphicsmagick
void magick_init();
#endif

int resize(std::string file, std::string ofile, ImageSize size, int quality, Unsharp* sharp, bool thumb = 0);
