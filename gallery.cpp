#define _LIBCPP_ENABLE_CXX17_REMOVED_AUTO_PTR

#include <iostream>
#include <filesystem>
#include <string>
#include <unistd.h>

#include "album.h"
#include "debug.h"
#include "file_ops.h"

int main(int argc, char** argv) {
	std::filesystem::path in_dir = "";
	std::filesystem::path out_dir = "html";
	std::filesystem::path update_dir;
	bool sync_gallery = 0;
	bool check_files = 0;
	bool delete_files = 0;
	bool dry_run = 0;

	int c;

	while ((c = getopt(argc, argv, "o:u:d:ncsph")) != -1)
		switch (c) {
			case 'd':
				try {
					if (std::stoi(optarg) > 0) {
						Debug(1) << "Setting debug level to " << optarg;
						Debug::set_debug_level(std::stoi(optarg));
					}
				}
				catch (...) {
					std::cout << "Option \"-d\" requires a valid number as an argument\n";
					std::exit(EXIT_FAILURE);
				}
				break;
			case 'o':
				out_dir = optarg;
				break;
			case 'u':
				update_dir = optarg;
				break;
			case 's':
				sync_gallery = 1;
				break;
			case 'p':
				Album::disable_openmp();
				break;
// 			case 'C':
// 				delete_files = 1;
// 				break;
			case 'c':
				check_files = 1;
				break;
			case 'n':
				dry_run = 1;
				break;
			case 'h':
			case '?':
			default:
				std::cout << "Usage: [-spnch] [-o output_dir] [-u update_dir] [-d level] [input_dir]\n";
				std::cout << "Generate html for photo gallery from structure in 'input_dir' in 'output_dir' according to options in 'album.dat' files.\n\n";
				std::cout << "Options:\n";
				std::cout << "\tinput_dir\tlocation of the gallery input structure (default: current directory)\n";
				std::cout << "\t-o output_dir\tuse specified output directory (default: 'html')\n";
				std::cout << "\t-u update_dir\tprocess only specified directory\n";
				std::cout << "\t-d level\tset debug level\n";
				std::cout << "\t-n\t\tdry run\n";
				std::cout << "\t-s\t\tsync with server by executing 'sync' script\n";
				std::cout << "\t-p\t\tdisable OpenMP multithreaded image processing\n";
				std::cout << "\t-c\t\tcheck for orphaned files in the output directory\n";
				// std::cout << "\t-C\t\tdelete unneeded files in the output directory\n";
				std::cout << "\t-h\t\tdisplay this help screen\n";
				std::exit(EXIT_FAILURE);
		}

	if (optind < argc)
		in_dir = argv[optind];

	Album* album = nullptr;

	if (!update_dir.empty()) {
		if (std::filesystem::is_directory(in_dir / update_dir)) {
			Debug(1) << "Processing update directory " << in_dir / update_dir;
			album = new Album(nullptr, in_dir, update_dir);
		} else {
			Debug(1) << "Update directory " << update_dir << " not found.";
			std::exit(EXIT_FAILURE);
		}
	} else {
		Debug(1) << "Processing directory " << (in_dir.empty() ? "." : in_dir);
		album = new Album(nullptr, in_dir);
	}

	if (!dry_run) album->generate(out_dir);
	if ((check_files || delete_files) && !dry_run) {
		std::set<std::filesystem::path> files;
		if (!update_dir.empty())
			files = find_files(out_dir / update_dir);
		else
			files = find_files(out_dir);
		std::cout << "Orphaned files:\n";
		for (auto f : files)
			if (!Album::needed_files.count(f)) std::cout << f << '\n';
	}

	delete album;

	if (sync_gallery && !dry_run) {
		if (std::filesystem::exists("sync")) {
			Debug(1) << "Syncing with server";
			std::cout.flush();
			if (system("./sync") < 0) Debug(1) << "Error running \"sync\".";
		} else {
			Warn() << "File \"sync\" not found.";
		}
	}

	// for_each(al.settings->lenses.begin(), al.settings->lenses.end(),
	// [](auto x) { std::cout << x.first << " - " << x.second << "\n"; });
}
