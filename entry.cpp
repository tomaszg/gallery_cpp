#define _LIBCPP_ENABLE_CXX17_REMOVED_AUTO_PTR

#include <filesystem>
#include <fstream>
#include <chrono>
#include <numeric>
#include "NLTemplate/NLTemplate.h"

#include "entry.h"
#include "album.h"
#include "exif.h"
#include "debug.h"
#include "magick.h"
#include "file_ops.h"
#include "cache.h"

std::string Entry::colspan(int n) {
	if (n > 1)
		return "colspan=\"" + std::to_string(n) + "\"";
	else
		return "";
}

Break::Break(Album* parent_ptr, const std::string& txt) {
	parent = parent_ptr;
	title = txt;
}

Break_start::Break_start(Album* parent_ptr, const std::string& txt) {
	parent = parent_ptr;
	title = txt;
}

Break_end::Break_end(Album* parent_ptr) {
	parent = parent_ptr;
}

Link::Link(Album* parent_ptr, const std::string& name, std::string url) : url(url) {
	Debug(3) << "Link constructor: " << name << ", " << url;

	if (!url.empty() && url.back() == '/') url.pop_back();

	parent = parent_ptr;
	title = name;
	std::string base = url;
	std::string n = url;
	int i = url.find_last_of('/');
	if (i >= 0) {
		base.erase(i, base.size() - i);
		n.erase(0, i);
		if (n.length() > 5 && (n.substr(n.length() - 5, 5)) == ".html")
			n.erase(n.end() - 5, n.end());
		thumb = base + "/thumbs" + n + parent->settings->thumb_format;
	} else
		Warn() << "Incorrect link: " << url;
}

Image::Image(Album* parent_ptr, std::string file, int num) {
	Debug(3) << "Image constructor: " << file;

	parent = parent_ptr;
	filename = file;
	number = num;

	if (parent->parent) directory = parent->directory / parent->filename;
	thumb_location = parent->settings->thumbs_dir / filename.stem();
	thumb_location += parent->settings->thumb_format;
	image_location = parent->settings->images_dir / filename;
	html_name = filename;
	html_name.replace_extension(".html");

	timestamp = (std::chrono::time_point_cast<std::chrono::milliseconds>(std::filesystem::last_write_time(file))).time_since_epoch().count();

	file += ".txt";
	std::ifstream txtfile;

	Debug(6) << "txt file: " << file;
	txtfile.open(file);
	if (txtfile.is_open()) {
		Debug(4) << "Found txt file for: " << filename << '.';
		std::stringstream buffer;
		buffer << txtfile.rdbuf();
		title = buffer.str();
	}
}

void Image::read_exif() {
	if (parent->settings->options.exif) {
		exif_db_data e;
		if (parent->exif_cache->get_exif(filename, &e))
			exif.read(this, e);
		else {
			exif.read(this);
			parent->exif_cache->insert_or_assign(filename, exif.get_rawdata());
		}
	}
}

int Image::create_thumb() {
	ImageSize size;

	Debug(4) << "Creating thumb for " << filename;

	std::string ifile = parent->gallery_root / directory / filename;
	std::string ofile = parent->target_location / thumb_location;

	if (parent->settings->local_thumb_size.width > 0 && parent->settings->local_thumb_size.height > 0)
		size = parent->settings->local_thumb_size;
	else
		size = parent->settings->thumb_size;

	cache_data cache_item = { ifile, thumb_location, size, parent->settings->thumb_quality, parent->settings->unsharp, timestamp };

	if (parent->thumbs_cache->check(&cache_item))
		return 0;

	int res = resize(ifile, ofile, size, parent->settings->thumb_quality, &parent->settings->unsharp);
	if (res == 0) {
		parent->thumbs_cache->insert_or_assign(cache_item);
	}
	return res;
}

int Entry::create_thumb() {
	return 0;
}

int Image::create_image() {
	std::string ifile = parent->gallery_root / directory / filename;
	std::string ofile = parent->target_location / image_location;

	if (parent->settings->options.conv) {
		ImageSize size;

		Debug(4) << "Creating image for " << filename;

		if (parent->settings->local_image_size.width > 0 && parent->settings->local_image_size.height > 0)
			size = parent->settings->local_image_size;
		else
			size = parent->settings->image_size;

		cache_data cache_item = { ifile, image_location, size, parent->settings->image_quality, parent->settings->unsharp, timestamp };

		if (parent->images_cache->check(&cache_item))
			return 0;

		int res = resize(ifile, ofile, size, parent->settings->image_quality, &parent->settings->unsharp, 1);
		if (res == 0) {
			parent->images_cache->insert_or_assign(cache_item);
		}
		return res;
	} else {
		if (compare_file_timestamp(ifile, ofile)) {
			Debug(4) << "File for image " << filename << " already exists and is up to date";
		} else {
			Debug(4) << "Copying image for " << filename;
			copyfile(ifile, ofile);
		}
		return 0;
	}
}

std::string Entry::highlight() const {
	return "";
}

std::string Link::highlight() const {
	return thumb;
}

std::string Image::highlight() const {
	return directory / filename;
}

void Entry::generate_html() {}

void Image::generate_html() {
	NL::Template::LoaderFile loader;
	NL::Template::Template t(loader);
	try {
		t.load(parent->settings->theme / "image.tpl");
	}
	catch (std::string& e) {
		Warn() << e;
		std::exit(EXIT_FAILURE);
	}

	try {
		t.set("charset", parent->settings->charset);
		t.set("keywords", parent->meta_keywords());
		t.set("style", parent->style_link());

		if (!parent->settings->rss_base.empty()) {
			t.block("rss").set("rss", parent->settings->rss_base / parent->settings->rss_file);
			t.block("rss2").set("rss", parent->settings->rss_base / parent->settings->rss_file);
		} else {
			t.block("rss").disable();
			t.block("rss2").disable();
		}

		t.block("contents_rel").set("url", "index.html");
		t.block("script").block("up").set("url", "index.html");
		t.block("up").set("url", "index.html");
		t.block("up").set("up", parent->settings->link_up);
		t.block("up_dis").disable();

		if (number > 1) {
			t.block("prev_rel").set("url", parent->images[number - 2]->html_name);
			t.block("script").block("prev").set("url", parent->images[number - 2]->html_name);
			t.block("prev").set("url", parent->images[number - 2]->html_name);
			t.block("prev").set("prev", parent->settings->link_prev);
			t.block("prev_dis").disable();
		} else {
			t.block("prev_rel").disable();
			t.block("script").block("prev").disable();
			t.block("prev").disable();
			t.block("prev_dis").set("prev", parent->settings->link_prev);
		}
		if (number < parent->n_images) {
			t.block("next_rel").set("url", parent->images[number]->html_name);
			t.block("script").block("next").set("url", parent->images[number]->html_name);
			t.block("next").set("url", parent->images[number]->html_name);
			t.block("next").set("next", parent->settings->link_next);
			t.block("next_dis").disable();
			t.block("next_img").set("url", parent->images[number]->html_name);
		} else {
			t.block("next_rel").disable();
			t.block("script").block("next").disable();
			t.block("next_dis").set("next", parent->settings->link_next);
			t.block("next").disable();
			t.block("next_img").disable();
			t.block("next_img2").disable();
		}

		t.set("title", parent->title);
		t.block("date").set("date", exif.formatted_date);
// 		t.set("cs", "");
//		t.set("cs2", "");
		t.set("parent", parent->parent_links(1) + " (" + std::to_string(number) + '/' + std::to_string(parent->n_images) + ")");

		if (!parent->settings->header.empty()) {
			t.set("header", parent->settings->header);
		}

		t.block("about").disable();
		t.block("more").disable();
		t.block("stats").disable();
		t.set("thumb_size", "0"); // useless, added to avoid CSS error

		t.set("alt", "foto " + parent->title);
		t.set("src", image_location);

		if (exif.formatted_exif.empty()) {
			t.block("exif").disable();
		} else {
			t.block("exif").set("exif", exif.formatted_exif);
			t.block("exif").set("exif_url", parent->settings->exif_icon_url);
		}

		if (title.empty()) {
			t.block("text").disable();
		} else {
			t.block("text").set("text", title);
		}

		t.set("footer", parent->settings->footer);
		t.set("cs_foot", "colspan=\"2\"");

		if (parent->settings->istats.empty()) {
			t.block("istats").disable();
		} else {
			t.block("istats").set("istats", parent->settings->istats);
		}
	}
	catch (std::string& e) {
		Warn() << e;
		std::exit(EXIT_FAILURE);
	}

	HtmlOutput index(parent->target_location / html_name);
	t.render(index);
	index.update_file();
	Album::needed_files.insert(parent->target_location / html_name);
}

std::string Break::generate(const std::string& where __attribute__((unused)), int cols, int* cur_col) {
	Debug(3) << "Generating break";

	NL::Template::LoaderFile loader;
	NL::Template::Template t(loader);
	try {
		t.load(parent->settings->theme / "break_item.tpl");
	}
	catch (std::string& e) {
		Warn() << e;
		std::exit(EXIT_FAILURE);
	}

	try {
		if (cur_col && *cur_col == 1)
			if (t.check_block("newrow")) t.block("newrow").disable();

		if (title.empty()) {
			t.set("class", "break_empty");
			t.set("text", "&nbsp;");
		} else {
			t.set("class", "break");
			t.set("text", title);
		}
		t.set("cs", colspan(cols));
	}
	catch (std::string& e) {
		Warn() << e;
		std::exit(EXIT_FAILURE);
	}

	if (cur_col) *cur_col = cols + 1;

	std::stringstream out;
	t.render(out);
	return out.str();
}

std::string Break_start::generate(const std::string& where __attribute__((unused)), int cols, int* cur_col) {
	Debug(3) << "Generating break";

	NL::Template::LoaderFile loader;
	NL::Template::Template t(loader);
	try {
		t.load(parent->settings->theme / "break_start_item.tpl");
	}
	catch (std::string& e) {
		Warn() << e;
		std::exit(EXIT_FAILURE);
	}

	try {
		if (cur_col && *cur_col == 1)
			if (t.check_block("newrow")) t.block("newrow").disable();

		if (title.empty()) {
			t.set("text", "&nbsp;");
		} else {
			t.set("text", title);
		}
		t.set("cs", colspan(cols));
	}
	catch (std::string& e) {
		Warn() << e;
		std::exit(EXIT_FAILURE);
	}

	if (cur_col) *cur_col = cols + 1;

	std::stringstream out;
	t.render(out);
	return out.str();
}

std::string Break_end::generate(const std::string& where __attribute__((unused)), int cols, int* cur_col) {
	Debug(3) << "Generating break";

	NL::Template::LoaderFile loader;
	NL::Template::Template t(loader);
	try {
		t.load(parent->settings->theme / "break_end_item.tpl");
	}
	catch (std::string& e) {
		Warn() << e;
		std::exit(EXIT_FAILURE);
	}

	try {
		if (cur_col && *cur_col == 1)
			if (t.check_block("newrow")) t.block("newrow").disable();

		t.set("cs", colspan(cols));
	}
	catch (std::string& e) {
		Warn() << e;
		std::exit(EXIT_FAILURE);
	}

	if (cur_col) *cur_col = cols + 1;

	std::stringstream out;
	t.render(out);
	return out.str();
}

std::string Link::generate(const std::string& where __attribute__((unused)), int cols, int* cur_col) {
	Debug(3) << "Generating link";

	NL::Template::LoaderFile loader;
	NL::Template::Template t(loader);
	try {
		t.load(parent->settings->theme / "link_item.tpl");
	}
	catch (std::string& e) {
		Warn() << e;
		std::exit(EXIT_FAILURE);
	}

	try {
		if (cur_col && *cur_col <= cols) {
			if (t.check_block("newrow")) t.block("newrow").disable();
		} else if (cur_col)
			*cur_col = 1;
		t.set("url", url);
		t.set("src", highlight());
		t.set("alt", "foto " + htmlencode(title));
		t.set("text", htmlencode(title));
	}
	catch (std::string& e) {
		Warn() << e;
		std::exit(EXIT_FAILURE);
	}

	if (cur_col) (*cur_col)++;

	std::stringstream out;
	t.render(out);
	return out.str();
}

std::string Image::generate(const std::string& where __attribute__((unused)), int cols, int* cur_col) {
	Debug(3) << "Generating image " << filename;

	Album::needed_files.insert(parent->target_location / thumb_location);
	Album::needed_files.insert(parent->target_location / image_location);

	generate_html();

	NL::Template::LoaderFile loader;
	NL::Template::Template t(loader);
	try {
		t.load(parent->settings->theme / "img_item.tpl");
	}
	catch (std::string& e) {
		Debug(1) << e;
		std::exit(EXIT_FAILURE);
	}

	try {
		if (cur_col && *cur_col <= cols) {
			if (t.check_block("newrow")) t.block("newrow").disable();
		} else if (cur_col)
			*cur_col = 1;
		t.set("url", html_name.string());
		t.set("src", thumb_location.string());
		t.set("alt", "foto " + htmlencode(parent->title));
		t.set("keywords", std::accumulate(exif.keywords.begin(), exif.keywords.end(), std::string(""),
										  [](std::string a, const std::string& b) { return std::move(a) + ' ' + b; }));
		t.set("top", parent->top_link());
	}
	catch (std::string& e) {
		Debug(1) << e;
		std::exit(EXIT_FAILURE);
	}

	if (cur_col) (*cur_col)++;

	std::stringstream out;
	t.render(out);
	return out.str();
}
