#define _LIBCPP_ENABLE_CXX17_REMOVED_AUTO_PTR

#include <fstream>

#include "file_ops.h"
#include "debug.h"
#include "regex_match.h"

void HtmlOutput::update_file() {
	std::fstream f(output_file, std::ios_base::in);
	bool write = 0;

	if (f.fail() || str().length() != std::filesystem::file_size(output_file)) {
		write = 1;
	}

	if (!write) {
		seekg(0);
		write = !std::equal(std::istreambuf_iterator<char>(rdbuf()),
							std::istreambuf_iterator<char>(),
							std::istreambuf_iterator<char>(f.rdbuf()));
	}

	if (!write) return;

	f.close();
	f.open(output_file, std::ios_base::out | std::ios_base::trunc);
	seekg(0);
	f << rdbuf();
}

void createdir(std::filesystem::path filename) {
	if (std::filesystem::exists(filename)) {
		if (!std::filesystem::is_directory(filename)) {
			Warn() << "Can't continue, " << filename << " exists but is not a directory.";
			std::exit(EXIT_FAILURE);
		}
	} else {
		if (!std::filesystem::create_directory(filename)) {
			Warn() << "Can't create directory " << filename << ".";
			std::exit(EXIT_FAILURE);
		}
	}
}

void copyfile(std::filesystem::path ifile, std::filesystem::path ofile) {
	std::filesystem::copy(ifile, ofile, std::filesystem::copy_options::overwrite_existing);

	if (std::filesystem::is_directory(ofile))
		ofile = ofile / std::filesystem::path(ifile).filename();
	std::filesystem::last_write_time(ofile, std::filesystem::last_write_time(ifile));
}

bool compare_file_timestamp(std::filesystem::path ifile, std::filesystem::path ofile) {
	if (std::filesystem::is_directory(ofile))
		ofile = ofile / std::filesystem::path(ifile).filename();
	return std::filesystem::exists(ofile) && (std::filesystem::file_size(ofile) == std::filesystem::file_size(ifile)) && (std::filesystem::last_write_time(ofile) == std::filesystem::last_write_time(ifile));
}

std::set<std::filesystem::path> find_files(std::filesystem::path dir) {
	std::set<std::filesystem::path> files;

	for (auto f : std::filesystem::recursive_directory_iterator(dir))
		if (f.path().filename() != ".cache.json") files.insert(f);
	return files;
}

std::string htmlencode(const std::string& data) {
	std::string tmp;
	tmp.reserve(data.size() + 20);

	for (size_t i = 0; i != data.size(); i++) {
		switch (data[i]) {
			case '&':
				tmp += "&amp;";
				break;
			case '\"':
				tmp += "&quot;";
				break;
			case '\'':
				tmp += "&apos;";
				break;
				/*            case '<':
                                tmp += "&lt;";
                                break;
            case '>':
                                tmp += "&gt;";
                                break;*/
			default:
				tmp += data[i];
				break;
		}
	}

	return tmp;
}
