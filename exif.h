#pragma once

#include <ctime>

// forward declarations
class Image;
namespace Exiv2 {
class ExifData;
class XmpData;
} // namespace Exiv2

struct exif_db_data {
	std::string model;
	std::string shutter;
	float focal = 0;
	float aperture = 0;
	int64_t iso = 0;
	int64_t short_foc = 0;
	int64_t long_foc = 0;
	std::string lens_name;
	std::string metering;
	std::string expo_prog;
	float exp_comp = 0;
	std::string date;
	bool flash = 0;
	std::string subject;
};

class Exif_data {
public:
	std::string formatted_exif;
	std::string formatted_date;
	std::vector<std::string> keywords;

	void read(const Image* ptr);
	void read(const Image* ptr, const exif_db_data& data);

	exif_db_data get_rawdata() const;

	static void init();

private:
	const Image* image_ptr;
	Exiv2::ExifData* exifdata;
	Exiv2::XmpData* xmpdata;
	exif_db_data raw_data;

	static void handle_exiv_warning(int lvl, const char* msg);

	std::tm date_tm;

	int read_key(const std::string&, std::string*);
	int read_key(const std::string&, float*);
	int read_key(const std::string&, int64_t*);
	int read_key_xmp(const std::string& key, std::string* res);

	void show_data();
	void generate_strings();
	void update_keywords();
};
