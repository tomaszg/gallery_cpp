#pragma once

#include <vector>
#include <set>
#include <filesystem>

#include "entry.h"

class scaling_db;
class exif_db;

class Album : public Entry {
public:
	std::vector<Entry*> entries;
	std::vector<Image*> images;
	inline static std::vector<Image*> global_images;
	inline static std::vector<Album*> global_albums; //doesn't include top album
	inline static std::set<std::filesystem::path> needed_files;

	inline static std::filesystem::path gallery_root;
	static void disable_openmp() { use_openmp = 0; }

	int n_images = 0; // number of images
	int n_dirs = 0; // number of directories

	scaling_db* thumbs_cache = nullptr;
	scaling_db* images_cache = nullptr;
	exif_db* exif_cache = nullptr;

	std::filesystem::path target_location; // destination where html files for album will be placed
	std::filesystem::path thumb_location;

	Settings* settings;

	Album(Album* parent_ptr, std::filesystem::path dirname, std::filesystem::path update_dirname = ""); // konstruktor
	Album(const Album& a) = delete;
	Album& operator=(Album& a) = delete;
	~Album();

	std::string highlight() const override;
	int create_thumb() override;
	std::string generate(const std::string& where = "", int cols = 0, int* cur_col = 0) override;

	std::string parent_links(bool last_link = 0) const;
	std::string meta_keywords() const;
	std::string style_link() const;
	std::string top_link() const;

private:
	inline static bool use_openmp = 1;

	std::filesystem::path update; // update mode

	int read_datafile(); // Read album.dat file, sets variables accordingly and adds entries
	int add_entry(const std::string& file); // Create a new entry and adds it to entires vector
	void add_from_glob(const std::string& glob, const bool reverse = 0); // Add all files matching given glob, possibly in reverse order
	void check_files(); // Check if all files present in the directory were added by globs

	void generate_html() override;

	std::set<std::string> added_files;

	//std::string about_link();
	//std::string rss_entry();
	//std::string rss_tag();
	//rss_meta();
};
