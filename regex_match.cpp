#include "debug.h"
#include "regex_match.h"
#include "cache.h"

static std::smatch res;

bool match(const std::regex& rg, const std::string& line, const std::string& desc, std::string* var) {
	std::ostringstream buf;

	if (std::regex_match(line, res, rg)) {
		if (var) {
			if (res.size() == 2)
				*var = res[1].str();
			else
				return 0;
		}
		if (!desc.empty()) {
			buf << "Read " << desc;
			if (res.size() == 2) buf << ": " << res[1].str();
			Debug(2) << buf.str();
		}
		return 1;
	} else
		return 0;
}

bool match(const std::regex& rg, const std::string& line, const std::string& desc, std::filesystem::path* var) {
	if (std::regex_match(line, res, rg) && res.size() == 2) {
		*var = res[1].str();
		Debug(2) << "Read " << desc << ": " << *var;
		return 1;
	}
	return 0;
}

bool match(const std::regex& rg, const std::string& line, const std::string& desc, int* var) {
	if (std::regex_match(line, res, rg) && res.size() == 2) {
		*var = stoi(res[1].str());
		Debug(2) << "Read " << desc << ": " << *var;
		return 1;
	}
	return 0;
}

bool match(const std::regex& rg, const std::string& line, const std::string& desc, ImageSize* var) {
	if (std::regex_match(line, res, rg) && res.size() == 3) {
		*var = ImageSize(stoi(res[1].str()), stoi(res[2].str()));
		Debug(2) << "Read " << desc << ": " << *var;
		return 1;
	}
	return 0;
}

bool match(const std::regex& rg, const std::string& line, const std::string& desc, Unsharp* var) {
	if (std::regex_match(line, res, rg) && res.size() == 5) {
		*var = Unsharp(std::stod(res[1].str()), std::stod(res[2].str()), std::stod(res[3].str()), std::stod(res[4].str()));
		Debug(2) << "Read " << desc << ": " << *var;
		return 1;
	}
	return 0;
}

bool match_more(const std::string& line, Settings* settings) {
	if (std::regex_match(line, res, regex_more) && res.size() == 3) {
		settings->more_link = res[1].str();
		settings->more_name = res[2].str();
		Debug(2) << "Read MORE: " << res[1].str() << ' ' << res[2].str();
		return 1;
	}
	return 0;
}

bool match_lens(const std::string& line, Settings* settings) {
	if (std::regex_match(line, res, regex_lens) && res.size() == 4) {
		int f1 = stoi(res[1].str());
		int f2 = stoi(res[2].str());

		if (settings->lenses.count(Lens(f1, f2)))
			Debug(2) << "Redefining LENS: " << f1 << "-" << f2 << " = " << res[3].str();
		else
			Debug(2) << "Read LENS: " << f1 << "-" << f2 << " = " << res[3].str();
		settings->lenses.insert_or_assign(Lens(f1, f2), res[3].str());
		return 1;
	}
	return 0;
}

bool match_band(const std::string& line, Settings* settings) {
	if (std::regex_match(line, res, regex_band) && res.size() == 4) {
		if (settings->bands.count(res[1].str()))
			Debug(2) << "Redefining BAND: " << res[1].str();
		else
			Debug(2) << "Read BAND: " << res[1].str();

		settings->bands.insert_or_assign(res[1].str(), Band(res[3].str(), res[2].str()));
		return 1;
	}
	return 0;
}

bool match_glob(const std::string& line, std::string* glob, bool* reverse) {
	if (std::regex_match(line, res, regex_glob) && res.size() == 3) {
		*glob = res[2].str();
		*reverse = !res[1].str().empty();
		return 1;
	}
	return 0;
}

bool match_link(const std::string& line, std::string* text, std::string* url, bool* highlight) {
	if (std::regex_match(line, res, regex_link) && res.size() == 4) {
		*text = res[3].str();
		*url = res[2].str();
		*highlight = (!res[1].str().empty()) ? 1 : 0;
		return 1;
	}
	return 0;
}

bool match_entry(const std::string& line, std::string* file, bool* highlight) {
	if (std::regex_match(line, res, regex_single) && res.size() == 3) {
		*file = res[2].str();
		*highlight = (!res[1].str().empty()) ? 1 : 0;
		return 1;
	}
	return 0;
}
