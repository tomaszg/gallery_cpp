#pragma once

#include <filesystem>
#include <map>
#include <string>
#include <omp.h>

#include "settings.h"
#include "exif.h"

struct cache_data {
	std::filesystem::path source_file;
	std::string target_file;
	ImageSize size;
	int quality;
	Unsharp sharp;
	long long int timestamp;
};

bool operator==(const cache_data& lhs, const cache_data& rhs);

class cache_db {
protected:
	std::filesystem::path cache_file;
	bool dirty = 0;
	omp_lock_t lock;

public:
	cache_db();
	~cache_db();
};

class scaling_db : private cache_db {
private:
	std::map<std::string, cache_data> data;

public:
	explicit scaling_db(std::filesystem::path filename);
	~scaling_db();

	bool check(const cache_data* item);
	void insert_or_assign(const cache_data& obj);
};

class exif_db : private cache_db {
private:
	std::map<std::string, exif_db_data> data;

public:
	explicit exif_db(std::filesystem::path filename);
	~exif_db();

	bool get_exif(const std::string& filename, exif_db_data* obj);
	void insert_or_assign(const std::string& filename, const exif_db_data& obj);
};
