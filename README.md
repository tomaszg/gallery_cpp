Gallery - C++ program generating a static photo gallery
========

Static Web image gallery system. It is a C++ rewrite of Perl script [gallery](https://gitlab.com/tomaszg/gallery), which in turn was
a fork of unmaintained [sgallery](http://sgallery.sourceforge.net/) by Daniel Rychcik. C++ version is much faster and adds some features, but some aspects are yet to be ported, see [TODO](TODO.md).


Introduction
------------

This is neither "drag and drop" nor "plug and play" thing. There are few things to learn, config files to be written, you might need some additional software, etc. This is probably UNIX-only (or at least quite difficult to make it run under Windows). If you are looking for something that will "just work" and don't care that much about the details - it might not be what you want (however it might work for you). If you are a geek that likes the command-line, file-driven kind of stuff - read on :) 

To compile it requires:

*   C++17 compliler supporting std::filesystem (so far only GCC-8 and GCC-9). Support for OpenMP is highly recommended
*   GNU Make or Meson (at least 0.46.0) + Ninja build system
*   Exiv2 library
*   ImageMagick or GraphicsMagick Magick++ library
*   JSON for Modern C++: https://github.com/nlohmann/json

It bundles a slightly patched version of NLTemplate from https://www.catnapgames.com/2013/04/09/nltemplate-html-template-library-for-c/.

You can use Meson to build the project. Run `meson build; cd build; ninja`. You might need to set `CXX` variable to point to a correct compiler. It produces a single binary called `gallery`.

If you would like to see some real-life examples, head to <http://photosite.pl> to see the gallery of the original author of the script, or to my gallery [Tomasz Goliński](http://tomaszg.pl/).

Quick start
---------

* Create a hierarchy of images. Each directory will become an album and it is possible to nest them.
* Choose or write your own HTML template.
* Create main `album.dat` file (see the example file) specifying image sizes, number of columns and so on.
* Create `style.css` file by customizing the attached sample file.
* Add `album.dat` files to albums. It should consist at least of tags: `TITLE:`, `HIGHLIGHT:` and mask of files to be included (e.g. `+*jpg`). You may add some additional data, see the section below.
* Run the gallery binary. By default it creates a directory `html` with the gallery files (html, images, thumbnails, CSS).

See the [USAGE](USAGE.md) file for details.

(C) 2006 Daniel Rychcik (muflon /at/ ais /dot/ pl)
(C) 2006-2020 Tomasz Goliński (tomaszg@math.uwb.edu.pl)
