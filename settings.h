#pragma once

#include <string>
#include <map>
#include <filesystem>

class ImageSize {
public:
	int width, height;

	ImageSize(int w, int h) : width(w), height(h){};
	ImageSize() : width(0), height(0){};

	void clear() { width = height = 0; };
};

bool operator==(const ImageSize& lhs, const ImageSize& rhs);
std::ostream& operator<<(std::ostream&, const ImageSize&);

class Options {
public:
	bool exif = 0;
	bool conv = 1;
	bool hidden = 0;
	bool leaf = 0;
	bool count = 0;
	bool count_dir = 0;
};

class Lens {
public:
	int focal_short;
	int focal_long;

	Lens(int foc1, int foc2);
};

std::ostream& operator<<(std::ostream&, const Lens&);

// Helper class for map of class Lens
class LensCmp {
public:
	bool operator()(const Lens& lhs, const Lens& rhs) const {
		if (lhs.focal_short < rhs.focal_short)
			return 1;
		if (lhs.focal_short > rhs.focal_short)
			return 0;
		return lhs.focal_long < rhs.focal_long;
	}
};

class Band {
public:
	std::string name;
	std::string url;

	Band(const std::string& n, const std::string& link);
};

class Unsharp {
public:
	double radius;
	double sigma;
	double amount;
	double threshold;

	explicit Unsharp(double rad = 0, double sig = 1, double am = 1, double thres = 0.05);
};

bool operator==(const Unsharp& lhs, const Unsharp& rhs);
std::ostream& operator<<(std::ostream&, const Unsharp&);

class Settings {
public:
	std::filesystem::path datafile = "album.dat";
	std::filesystem::path theme = "simple-template";
	std::string charset = "ISO-8859-2";
	std::filesystem::path images_dir = "images";
	std::filesystem::path thumbs_dir = "thumbs";
	std::string default_album_title = "Untitled";
	std::string link_up = "Up";
	std::string link_prev = "Previous";
	std::string link_next = "Next";
	// std::string link_about = "About the author";
	std::string link_rss = "RSS feed";
	std::filesystem::path rss_file = "rss.xml";
	std::string tree_separator = "&diams;";
	std::string exif_icon_url = "http://tomaszg.pl/foto/info.gif";
	std::string date_format = "%e-%m-%Y %R";

	std::string footer;
	std::string header;
	Options options;
	// bool force_images = 0;
	std::map<Lens, std::string, LensCmp> lenses;
	std::map<std::string, Band> bands;
	std::string highlight;
	std::filesystem::path css_file = "style.css";
	std::filesystem::path local_css_file;
	// std::string about_file;
	// std::string table_width = "995px";
	ImageSize image_size = ImageSize(900, 600);
	ImageSize local_image_size;
	ImageSize album_size = ImageSize(300, 200);
	ImageSize local_album_size;
	ImageSize thumb_size = ImageSize(231, 154);
	ImageSize local_thumb_size;
	std::string thumb_format = ".jpg";
	int columns = 3;
	int local_columns = 0;
	std::string meta_keywords;
	std::string local_meta_keywords;
	// float gamma = 1;
	Unsharp unsharp;
	int image_quality = 90;
	int thumb_quality = 80;
	std::string rss_base;
	std::string istats;
	std::string more_link;
	std::string more_name;

	// produces a copy of object with local fields set to default
	Settings inherit() const;
};
