#define _LIBCPP_ENABLE_CXX17_REMOVED_AUTO_PTR

#include <fstream>
#include <glob.h>
#include "NLTemplate/NLTemplate.h"

#include "album.h"
#include "debug.h"
#include "magick.h"
#include "file_ops.h"
#include "regex_match.h"
#include "cache.h"

Album::Album(Album* parent_ptr, std::filesystem::path dirname, std::filesystem::path update_dirname) : update(update_dirname) {
	parent = parent_ptr;
	settings = new Settings;
	std::filesystem::path cwd;

	if (parent) {
		filename = dirname;
		if (parent->parent) directory = parent->directory / parent->filename;
		*settings = parent->settings->inherit();
	} else {
		gallery_root = dirname;
	}

	Debug(3) << "Album constructor: " << dirname;

	cwd = std::filesystem::current_path();
	if (!dirname.empty()) {
		std::filesystem::current_path(dirname);
		Debug(2) << "Changing directory to: " << dirname;
	}

	read_datafile();

	if (title.empty()) title = settings->default_album_title;

	if (parent) thumb_location = parent->settings->thumbs_dir / (std::string(filename) + parent->settings->thumb_format);

	if (settings->more_link.empty() && settings->bands.count(filename)) {
		const auto& i = settings->bands.at(filename);
		settings->more_link = i.url;
		settings->more_name = i.name;
	}

	exif_cache = new exif_db(cwd / dirname);

	std::filesystem::current_path(cwd);
	Debug(2) << "Changing directory back to: " << cwd;
}

Album::~Album() {
	delete settings;
	delete thumbs_cache;
	delete images_cache;
	delete exif_cache;

	for (auto i : entries)
		delete i;
	entries.clear();
}

int Album::read_datafile() {
	std::ifstream datafile;
	std::string line;

	datafile.open(settings->datafile);
	if (!datafile.is_open()) {
		Debug(1) << "Datafile: " << settings->datafile << "not found, adding all files.";
		add_from_glob("*", 0);
		return -1;
	}

	std::string res_str, res_str2;
	bool res_bool;
	int res_int;
	bool found_glob = 0;

	while (getline(datafile, line)) {
		if (match(regex_comment, line)) continue;
		if (match(regex_command, line)) {
			if (match(regex_title, line, "TITLE", &title)) continue;
			if (match(regex_date, line, "DATE", &date)) continue;
			if (match(regex_date_format, line, "DATE_FORMAT", &settings->date_format)) continue;
			// if (match(regex_about, line, "ABOUT", &settings->about_file)) continue;
			if (match(regex_theme, line, "THEME", &settings->theme)) continue;
			if (match(regex_charset, line, "CHARSET", &settings->charset)) continue;
			if (match(regex_lcss, line, "LOCAL_CSS", &settings->local_css_file)) continue;
			if (match(regex_css, line, "CSS", &settings->css_file)) continue;
			if (match(regex_highlight, line, "HIGHLIGHT", &settings->highlight)) continue;
			if (match(regex_rss_base, line, "RSS_BASE", &settings->rss_base)) continue;
			if (match(regex_lcols, line, "LOCAL_COLUMNS", &settings->local_columns)) continue;
			if (match(regex_cols, line, "COLUMNS", &settings->columns)) continue;
			if (match(regex_limsize, line, "LOCAL_IMAGE_SIZE", &settings->local_image_size)) continue;
			if (match(regex_imsize, line, "IMAGE_SIZE", &settings->image_size)) continue;
			if (match(regex_lalsize, line, "LOCAL_ALBUM_SIZE", &settings->local_album_size)) continue;
			if (match(regex_alsize, line, "ALBUM_SIZE", &settings->album_size)) continue;
			if (match(regex_lthsize, line, "LOCAL_THUMB_SIZE", &settings->local_thumb_size)) continue;
			if (match(regex_thsize, line, "THUMB_SIZE", &settings->thumb_size)) continue;
			if (match(regex_thformat, line, "THUMB_FORMAT", &settings->thumb_format)) {
				if (!settings->thumb_format.empty() && settings->thumb_format.at(0) != '.')
					settings->thumb_format.insert(0, 1, '.');
				continue;
			}
			if (match(regex_imq, line, "IMAGE_QUALITY", &settings->image_quality)) continue;
			if (match(regex_thq, line, "THUMB_QUALITY", &settings->thumb_quality)) continue;
			if (match(regex_lmeta, line, "LOCAL_META_KEYWORDS", &settings->local_meta_keywords)) continue;
			if (match(regex_meta, line, "META_KEYWORDS", &settings->meta_keywords)) continue;
			if (match(regex_footer, line, "FOOTER", &settings->footer)) continue;
			if (match(regex_header, line, "HEADER", &settings->header)) continue;
			if (match(regex_istats, line, "ISTATS", &settings->istats)) continue;
			if (match(regex_unsharp, line, "UNSHARP", &settings->unsharp)) continue;
			if (match(regex_debug, line, "DEBUG", &res_int)) {
				Debug::set_debug_level(res_int);
				continue;
			}
			if (match_more(line, settings)) continue;
			if (match_lens(line, settings)) continue;
			if (match_band(line, settings)) continue;

			if (match(regex_break, line, "BREAK", &res_str)) {
				Break* b = new Break(this, res_str);
				entries.push_back(b);
				continue;
			}

			if (match(regex_break_start, line, "BREAK START", &res_str)) {
				Break_start* b = new Break_start(this, res_str);
				entries.push_back(b);
				continue;
			}

			if (match(regex_break_end, line, "BREAK END")) {
				Break_end* b = new Break_end(this);
				entries.push_back(b);
				continue;
			}

			if (match(regex_options, line, "OPTIONS", &res_str)) {
				if (match(regex_opt_noexif, res_str, "noexif - disabled EXIF data display"))
					settings->options.exif = 0;
				else if (match(regex_opt_exif, res_str, "exif - enabled EXIF data display"))
					settings->options.exif = 1;

				if (match(regex_opt_noconv, res_str, "noconv - enabled direct image copy"))
					settings->options.conv = 0;
				else if (match(regex_opt_conv, res_str, "conv - disabled direct image copy"))
					settings->options.conv = 1;

				if (match(regex_opt_hidden, res_str, "hidden - hiding this album in the upper-level listing")) settings->options.hidden = 1;
				if (match(regex_opt_leaf, res_str, "leaf - include this album in the RSS")) settings->options.leaf = 1;
				if (match(regex_opt_count_dir, res_str, "count_dir - print directory count in album listing")) settings->options.count_dir = 1;
				if (match(regex_opt_count, res_str, "count - print image count in album listing")) settings->options.count = 1;

				continue;
			}
			Warn() << "Unrecognized command: " << line << " in " << directory / filename;
			continue;
		}

		if (match_glob(line, &res_str, &res_bool)) {
			add_from_glob(res_str, res_bool);
			found_glob = 1;
			continue;
		}

		if (match_link(line, &res_str, &res_str2, &res_bool)) {
			Debug(3) << "Adding link: " << res_str << " -> " << res_str2;
			Link* l = new Link(this, res_str, res_str2);
			entries.push_back(l);
			if (res_str2.length() > 5 && (res_str2.substr(res_str2.length() - 5, 5)) == ".html")
				n_images++;
			else
				n_dirs++;
			if (res_bool)
				settings->highlight = l->highlight();
			continue;
		}

		if (match_entry(line, &res_str, &res_bool)) {
			add_entry(res_str);
			if (res_bool)
				settings->highlight = entries.back()->highlight();
			continue;
		}
	}

	if (found_glob) check_files();
	return 0;
}

int Album::add_entry(const std::string& file) {
	Entry* ent;

	// skip files which have different purpose
	if (file == settings->datafile || file == settings->css_file || file == settings->local_css_file || file.empty() || (file.size() > 4 && file.substr(file.size() - 4, 4) == ".txt")) return -1;

	if (!update.empty() && !std::filesystem::equivalent(file, update)) {
		Debug(2) << "Skipping entry: " << file << " due to update mode";
		return -1;
	}

	Debug(3) << "Adding entry: " << file;

	if (std::filesystem::exists(file)) {
		if (std::filesystem::is_directory(file)) {
			Album* al = new Album(this, file);
			ent = al;
			global_albums.push_back(al);
			if (!al->settings->options.hidden) n_dirs++;
		} else if (std::filesystem::is_regular_file(file)) {
			Image* im = new Image(this, file, ++n_images);
			ent = im;
			images.push_back(im);
			global_images.push_back(im);
			if (added_files.contains(file))
				Warn() << "File " << file << "added twice in " << directory / filename;
			added_files.insert(file);
		} else {
			Warn() << "Unknown entry: " << file;
			return -1;
		}
	} else {
		Warn() << "Can't open: " << file;
		return -1;
	}

	entries.push_back(ent);
	return 0;
}

void Album::add_from_glob(const std::string& glob, const bool reverse) {
	glob_t globbuf;
	int res;

	Debug(2) << "Including from mask: " << glob << (reverse ? " rev" : "");
	res = ::glob(glob.c_str(), 0, nullptr, &globbuf);
	if (res == 0) {
		for (size_t i = 0; i < globbuf.gl_pathc; i++) {
			add_entry(globbuf.gl_pathv[reverse ? globbuf.gl_pathc - i - 1 : i]);
		}
		globfree(&globbuf);
	} else if (res == GLOB_NOMATCH)
		Debug(1) << "Empty result of glob " << glob << " in " << filename;
	else
		Warn() << "Error performing glob " << glob << " in " << filename;
}

void Album::check_files() {
	glob_t globbuf;
	int res;

	res = ::glob("*", 0, nullptr, &globbuf);
	if (res != 0)
		return;
	for (size_t i = 0; i < globbuf.gl_pathc; i++) {
		std::string file = globbuf.gl_pathv[i];
		if (file == settings->datafile || file == settings->css_file || file == settings->local_css_file || file.empty() || (file.size() > 4 && file.substr(file.size() - 4, 4) == ".txt")) continue;
		if (!std::filesystem::exists(file) || !std::filesystem::is_regular_file(file)) continue;
		if (!added_files.contains(file)) Warn() << "Omitted file: " << file << " in " << directory / filename;
	}
	globfree(&globbuf);
}

std::string Album::highlight() const {
	if (!settings->highlight.empty()) {
		if (std::filesystem::is_regular_file(gallery_root / directory / filename / settings->highlight))
			return directory / filename / settings->highlight;
		if (std::filesystem::is_directory(gallery_root / directory / filename / settings->highlight)) {
			for (auto i : entries) {
				if (i->filename == settings->highlight) return i->highlight();
			}
		}
		Warn() << "Wrong highlight entry for album " << title << ": " << directory / filename;
	}

	for (auto i : entries)
		if (!(i->highlight().empty())) {
			return i->highlight();
		}

	return "";
}

int Album::create_thumb() {
	ImageSize size;

	std::string ifile = gallery_root / highlight();
	std::string ofile = parent->target_location / thumb_location;

	if (parent->settings->local_album_size.width > 0 && parent->settings->local_album_size.height > 0)
		size = parent->settings->local_album_size;
	else
		size = parent->settings->album_size;

	cache_data cache_item = { ifile, filename, size, parent->settings->thumb_quality, parent->settings->unsharp, 0 };

	if (thumbs_cache->check(&cache_item))
		return 0;

	int res = resize(ifile, ofile, size, parent->settings->thumb_quality, &parent->settings->unsharp, 1);
	if (res == 0) {
		thumbs_cache->insert_or_assign(cache_item);
	}
	return res;
}

std::string Album::parent_links(bool last_link) const {
	const Album* ptr = parent;
	std::string res;

	if (last_link)
		res = "<a href=\"index.html\">" + title + "</a>\n";
	else
		res = title + '\n';

	std::string n = "../";
	while (ptr) {
		res = " <a href=\"" + n + "\">" + ptr->title + "</a> " + settings->tree_separator + ' ' + res;
		ptr = ptr->parent;
		n += "../";
	}
	return res;
}

std::string Album::top_link() const {
	const Album* ptr = parent;
	std::string res;

	while (ptr) {
		res += "../";
		ptr = ptr->parent;
	}
	return res;
}

std::string Album::style_link() const {
	// TODO should we do real cascade like original code did?
	std::string res;

	res = "  <link rel=\"stylesheet\" type=\"text/css\" href=\"" + top_link() + std::string(settings->css_file.filename()) + "\">\n";

	if (!settings->local_css_file.empty()) {
		res += "  <link rel=\"stylesheet\" type=\"text/css\" href=\"" + std::string(settings->local_css_file.filename()) + "\">\n";
	}

	return res;
}

std::string Album::meta_keywords() const {
	const Album* ptr = this;

	std::string res;

	while (ptr) {
		if (!ptr->title.empty()) res += ptr->title + ',';
		ptr = ptr->parent;
	}
	if (!settings->meta_keywords.empty())
		res += settings->meta_keywords;
	if (!settings->local_meta_keywords.empty())
		res += ',' + settings->local_meta_keywords;
	return res;
}

std::string Album::generate(const std::string& where, int cols, int* cur_col) {
	if (n_images == 0 && n_dirs == 0) {
		Warn() << "No images or directories, can't create an album " << filename;
		std::exit(EXIT_FAILURE);
	}

	if (!where.empty())
		target_location = where;
	else
		target_location = parent->target_location / filename;

	Debug(2) << "Generating album " << filename << " in " << target_location;

	createdir(target_location);
	needed_files.insert(target_location);
	createdir(target_location / settings->thumbs_dir);
	needed_files.insert(target_location / settings->thumbs_dir);
	thumbs_cache = new scaling_db(target_location / settings->thumbs_dir);
	if (n_images > 0) {
		createdir(target_location / settings->images_dir);
		needed_files.insert(target_location / settings->images_dir);
		images_cache = new scaling_db(target_location / settings->images_dir);
	}

	// copy css file if necessary
	std::filesystem::path css_filename = gallery_root / directory / filename / settings->css_file;
	if (!parent && !settings->css_file.empty() && std::filesystem::exists(css_filename)) {
		if (!compare_file_timestamp(css_filename, target_location))
			copyfile(css_filename, target_location);
		needed_files.insert(target_location / css_filename);
	}
	css_filename = gallery_root / directory / filename / settings->local_css_file;
	if (!settings->local_css_file.empty() && std::filesystem::exists(css_filename)) {
		if (!compare_file_timestamp(css_filename, target_location))
			copyfile(css_filename, target_location);
		needed_files.insert(target_location / css_filename);
	}
	if (parent) needed_files.insert(parent->target_location / thumb_location);

	// read EXIF and generate thumbs/images in parallel for all entries
	if (!parent) {
		Debug(1) << "Reading Exif data for all images";
		std::cout.flush();
		Exif_data::init();

#pragma omp parallel for if (use_openmp)
		for (int i = 0; i < static_cast<int>(global_images.size()); i++) {
			if (global_images[i]->parent->settings->options.exif)
				global_images[i]->read_exif();
		}
	}

	generate_html();

	if (!parent) {
#ifdef graphicsmagick
		magick_init();
#endif
		Debug(1) << "Generating thumbs and images for all images";
		std::cout.flush();
#pragma omp parallel for schedule(dynamic) if (use_openmp)
		for (int i = 0; i < static_cast<int>(global_images.size()); i++) {
			global_images[i]->create_thumb();
			global_images[i]->create_image();
			// entries[i]->generate_html();
		}
		Debug(1) << "Generating thumbs for all albums";
		std::cout.flush();
#pragma omp parallel for schedule(dynamic) if (use_openmp)
		for (int i = 0; i < static_cast<int>(global_albums.size()); i++) {
			global_albums[i]->create_thumb();
		}
		return "";
	}

	if (settings->options.hidden)
		return "";

	NL::Template::LoaderFile loader;
	NL::Template::Template t(loader);
	try {
		t.load(parent->settings->theme / "album_item.tpl");
	}
	catch (std::string& e) {
		Warn() << e;
		std::exit(EXIT_FAILURE);
	}

	try {
		if (*cur_col <= cols) {
			if (t.check_block("newrow")) t.block("newrow").disable();
		} else
			*cur_col = 1;
		t.set("filename", filename / "index.html");
		t.set("thumb", thumb_location);
		t.set("alt", "foto " + htmlencode(title));
		std::string caption = htmlencode(title);
		if (!title.empty()) {
			if (parent->settings->options.count && parent->settings->options.count_dir) {
				if (n_images > n_dirs)
					caption += " [" + std::to_string(n_images) + "]";
				else
					caption += " (" + std::to_string(n_dirs) + ")";
			} else {
				if (parent->settings->options.count)
					caption += " [" + std::to_string(n_images) + "]";
				if (parent->settings->options.count_dir)
					caption += " (" + std::to_string(n_dirs) + ")";
			}
		}
		t.set("caption", caption);
	}
	catch (std::string& e) {
		Warn() << e;
		std::exit(EXIT_FAILURE);
	}

	if (cur_col) (*cur_col)++;

	std::stringstream out;
	t.render(out);
	return out.str();
}

void Album::generate_html() {
	int columns;
	if (settings->local_columns > 0)
		columns = settings->local_columns;
	else
		columns = settings->columns;

	NL::Template::LoaderFile loader;
	NL::Template::Template t(loader);
	try {
		t.load(settings->theme / "album.tpl");
	}
	catch (std::string& e) {
		Warn() << e;
		std::exit(EXIT_FAILURE);
	}

	try {
		t.set("charset", settings->charset);
		t.set("keywords", meta_keywords());
		t.set("style", style_link());
		t.set("top", top_link());

		if (!settings->rss_base.empty()) {
			t.block("rss").set("rss", settings->rss_base / settings->rss_file);
			t.block("rss2").set("rss", settings->rss_base / settings->rss_file);
		} else {
			t.block("rss").disable();
			t.block("rss2").disable();
		}

		if (parent) {
			t.block("contents_rel").set("url", "../index.html");
			t.block("script").block("up").set("url", "../index.html");
			t.block("up").set("url", "../index.html");
			t.block("up").set("up", settings->link_up);
			t.block("up_dis").disable();
		} else {
			t.block("contents_rel").disable();
			t.block("script").disable();
			t.block("up").disable();
			t.block("up_dis").set("up", settings->link_up);
		}
		t.block("prev_rel").disable();
		t.block("next_rel").disable();
		t.block("script").block("prev").disable();
		t.block("script").block("next").disable();
		t.block("prev").disable();
		t.block("next").disable();
		t.block("prev_dis").set("prev", settings->link_prev);
		t.block("next_dis").set("next", settings->link_next);
		t.block("date").disable();

		t.set("title", htmlencode(title));

		int size = 0;
		if (n_images > 0)
			size = std::max({ size, settings->local_thumb_size.height, settings->local_thumb_size.width, settings->thumb_size.height, settings->thumb_size.width });
		if (n_dirs > 0)
			size = std::max({ size, settings->local_album_size.height, settings->local_album_size.width, settings->album_size.height, settings->album_size.width });
		t.set("thumb_size", std::to_string(size) + "px");
		t.set("cs", colspan(columns));
		t.set("cs2", colspan(columns - 1));
		t.set("parent", parent_links());

		if (!settings->header.empty()) {
			t.set("header", settings->header);
		}

		if (std::filesystem::exists(directory / filename / "about.html")) {
		} else {
			t.block("about").disable();
		}

		if (settings->istats.empty()) {
			t.block("stats").disable();
		} else {
			t.block("stats").set("filename", filename.string());
		}

		if (settings->more_link.empty()) {
			t.block("more").disable();
		} else {
			t.block("more").set("more_link", settings->more_link);
			t.block("more").set("more_name", settings->more_name);
		}

		int cur_col = 1;
		std::stringstream body;
		for (auto i : entries)
			body << i->generate("", columns, &cur_col);

		t.set("body", body.str());
		t.set("footer", settings->footer);
		t.set("cs_foot", colspan(columns));

		if (settings->istats.empty()) {
			t.block("istats").disable();
		} else {
			t.block("istats").set("istats", settings->istats);
		}
	}
	catch (std::string& e) {
		Warn() << e;
		std::exit(EXIT_FAILURE);
	}

	HtmlOutput index(target_location / "index.html");
	t.render(index);

	if (update.empty())
		index.update_file();
	needed_files.insert(target_location / "index.html");
}
