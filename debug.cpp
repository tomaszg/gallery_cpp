#include "debug.h"

int Debug::debug_level = 2;

std::string FG_RED = "\033[31m";
// std::string FG_GREEN = "\033[32m";
// std::string FG_BLUE = "\033[34m";
// std::string FG_DEFAULT = "\033[39m";
// std::string BG_RED = "\033[41m";
// std::string BG_GREEN = "\033[42m";
// std::string BG_BLUE = "\033[44m";
std::string RESET = "\033[0m";

Debug::Debug(int n) : lvl(n) {
	if (lvl <= debug_level) {
		std::string indent;

		indent.append(lvl - 1, '\t');
		if (lvl > 1) indent.append("*** ");
		buffer << indent;
	}
}

Debug::~Debug() {
	if (lvl <= debug_level) {
		buffer << '\n';
#pragma omp critical(cout)
		std::cout << buffer.str();
	}
}

void Debug::set_debug_level(int n) { debug_level = n; }

Warn::Warn() : Debug(1) {
	buffer << FG_RED;
}

Warn::~Warn() {
	buffer << RESET;
}
