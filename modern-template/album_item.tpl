  <div class="thumb_album">
     <a href="{{ filename }}">
     <figure>
       <img class="thumb_album" src="{{ thumb }}" alt="{{ alt }}">
     <figcaption>
       {{ caption }}
     </figcaption>
     </figure>
     </a>
  </div>