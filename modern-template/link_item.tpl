  <div class="thumb_album">
     <a href="{{ url }}">
     <figure>
       <img class="thumb_image" src="{{ src }}" alt="{{ alt }}">
     <figcaption>
       {{ text }}
     </figcaption>
     </figure>
     </a>
  </div>
