  <div class="thumb_image">
     <a href="{{ url }}">
     <figure>
       <img class="thumb_image" src="{{ src }}" alt="{{ alt }}">
     </figure>
     </a>
  </div>
