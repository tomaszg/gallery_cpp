<!DOCTYPE html>
<html lang="pl">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset={{ charset }}">
  <meta name="revisit-after" content="2 days">
  <meta name="robots" content="all">
  <meta name="Keywords" content="{{ keywords }}">
{{ style }}
{% block rss %}
  <link rel="alternate" type="application/rss+xml" title="RSS" href="{{ rss }}">
{% endblock %}
{% block contents_rel %}
  <link rel="Contents" href="{{ url }}">
{% endblock %}
{% block next_rel %}
  <link rel="Next" href="{{ url }}">
{% endblock %}
{% block prev_rel %}
  <link rel="Prev" href="{{ url }}">
{% endblock %}
  <title>{{ title }}</title>
{% block script %}
  <script>
    <!--
      function getKey(event) {
        if (!event) event = window.event;
        if (event.keyCode) code = event.keyCode;
        else if (event.which) code = event.which;
        if (event.shiftKey) {
{% block prev %}
        if (code == 37) {
            document.location = '{{ url }}';
          }
{% endblock %}
{% block up %}
          if (code == 38) {
            document.location = '{{ url }}';
          }
{% endblock %}
{% block next %}
          if (code == 39) {
            document.location = '{{ url }}';
          }
{% endblock %}
        }
        return true;
      }
      document.onkeypress = getKey;
    // -->
  </script>
{% endblock %}
</head>
<body style="--thumb_size: {{ thumb_size }}">
<header>
<div class=header_txt>
{{ header }}
</div>
{% block date %}
<div class="date">{{ date }}</div>
{% endblock %}
<h1>{{ title }}</h1>
</header>
<nav>
<span>{{ parent }}</span>
<span class="flex_spacer"></span>
{% block about %}<span><a href="about.html">About</a></span>{% endblock %}
{% block stats %}<span><a href="/cgi-bin/stats2.cgi?{{ filename }}">Most Viewed</a></span>{% endblock %}
<span>{% block prev %}<a href="{{ url }}">{{ prev }}</a>{% endblock %}{% block prev_dis %}{{ prev }}{% endblock %}</span>
<span>{% block up %}<a href="{{ url }}">{{ up }}</a>{% endblock %}{% block up_dis %}{{ up }}{% endblock %}</span>
<span>{% block next %}<a href="{{ url }}">{{ next }}</a>{% endblock %}{% block next_dis %}{{ next }}{% endblock %}</span>
</nav>
     
{% block more %}
    <aside class="more_link"><a href="{{ more_link }}"  target="_blank">Więcej zdjęć {{ more_name }}</a></aside>
{% endblock %}

