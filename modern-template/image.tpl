{% include modern-template/header.tpl %}
  <figure class="image">
{% block next_img %}
     <a href="{{ url }}">
{% endblock %}
     <img alt="{{ alt }}" class="image" src="{{ src }}">
{% block next_img2 %}
     </a>
{% endblock %}
{% block text %}
    <figcaption>
    {{ text }}
    </figcaption>
{% endblock %}
  </figure>
{% block exif %}
    <details class="exif"><summary><img src="{{ exif_url }}" alt="Exif data"></summary>
     {{ exif }}
    </details>
{% endblock %}
{% include modern-template/footer.tpl %}
