#include <fstream>
#include <nlohmann/json.hpp>

#include "cache.h"
#include "regex_match.h"
#include "debug.h"

using json = nlohmann::json;

void to_json(json& j, const ImageSize& item) {
	j = json{ { "width", item.width }, { "height", item.height } };
}

void to_json(json& j, const Unsharp& item) {
	j = json{ { "radius", item.radius }, { "sigma", item.sigma }, { "amount", item.amount }, { "threshold", item.threshold } };
}

void to_json(json& j, const exif_db_data& item) {
	j = json{ { "model", item.model }, { "shutter", item.shutter }, { "focal", item.focal }, { "aperture", item.aperture }, { "iso", item.iso }, { "short_foc", item.short_foc }, { "long_foc", item.long_foc }, { "lens_name", item.lens_name }, { "metering", item.metering }, { "expo_prog", item.expo_prog }, { "exp_comp", item.exp_comp }, { "date", item.date }, { "flash", item.flash }, { "subject", item.subject } };
}

void to_json(json& j, const cache_data& item) {
	j = json{ { "target_file", item.target_file }, { "source_file", item.source_file }, { "size", item.size }, { "quality", item.quality }, { "unsharp", item.sharp }, { "timestamp", item.timestamp } };
}

void from_json(const json& j, ImageSize& item) {
	j.at("width").get_to(item.width);
	j.at("height").get_to(item.height);
}

void from_json(const json& j, Unsharp& item) {
	j.at("radius").get_to(item.radius);
	j.at("sigma").get_to(item.sigma);
	j.at("amount").get_to(item.amount);
	j.at("threshold").get_to(item.threshold);
}

void from_json(const json& j, exif_db_data& item) {
	j.at("model").get_to(item.model);
	j.at("shutter").get_to(item.shutter);
	j.at("focal").get_to(item.focal);
	j.at("aperture").get_to(item.aperture);
	j.at("iso").get_to(item.iso);
	j.at("short_foc").get_to(item.short_foc);
	j.at("long_foc").get_to(item.long_foc);
	j.at("lens_name").get_to(item.lens_name);
	j.at("metering").get_to(item.metering);
	j.at("expo_prog").get_to(item.expo_prog);
	j.at("exp_comp").get_to(item.exp_comp);
	j.at("date").get_to(item.date);
	j.at("flash").get_to(item.flash);
	if (j.contains("subject"))
		j.at("subject").get_to(item.subject);
}

void from_json(const json& j, cache_data& item) {
	j.at("target_file").get_to(item.target_file);
	j.at("source_file").get_to(item.source_file);
	j.at("size").get_to(item.size);
	j.at("quality").get_to(item.quality);
	j.at("unsharp").get_to(item.sharp);
	j.at("timestamp").get_to(item.timestamp);
}

bool operator==(const cache_data& lhs, const cache_data& rhs) {
	return (lhs.source_file == rhs.source_file) && (lhs.size == rhs.size) && (lhs.quality == rhs.quality) && (lhs.sharp == rhs.sharp) && (lhs.timestamp == rhs.timestamp);
}

cache_db::cache_db() {
	omp_init_lock(&lock);
}

cache_db::~cache_db() {
	omp_destroy_lock(&lock);
}

scaling_db::scaling_db(std::filesystem::path filename) {
	cache_file = filename / ".cache.json";

	std::ifstream datafile(cache_file);

	if (!datafile.is_open()) {
		Debug(3) << "Cache file: " << cache_file << " not found.";
		return;
	}

	try {
		json j;
		datafile >> j;

		for (auto& el : j.items()) {
			cache_data item = el.value().get<cache_data>();
			data.insert_or_assign(item.target_file, item);
			Debug(5) << "Read cache item: " << item.target_file << " - " << item.size << ' ' << item.sharp << ' ' << item.quality << ' ' << item.timestamp;
		}
	}
	catch (...) {
		Debug(1) << "Cache file: " << cache_file << " corrupted.";
		std::exit(EXIT_FAILURE);
	}
}

scaling_db::~scaling_db() {
	if (dirty && !data.empty()) {
		std::ofstream datafile(cache_file);
		json j(data);
		datafile << j;
	}
}

bool scaling_db::check(const cache_data* item) {
	std::string file = item->target_file;

	omp_set_lock(&lock);
	bool res = data.count(file) && data.at(file) == *item;
	omp_unset_lock(&lock);

	if (res)
		Debug(5) << "Cache hit: " << file;
	else
		Debug(5) << "Cache miss: " << file;

	return res;
}

void scaling_db::insert_or_assign(const cache_data& obj) {
	dirty = 1;
	omp_set_lock(&lock);
	data.insert_or_assign(obj.target_file, obj);
	omp_unset_lock(&lock);
}

exif_db::exif_db(std::filesystem::path filename) {
	std::ifstream datafile(filename / ".exif_cache.json");

	cache_file = filename / ".exif_cache.json";

	if (!datafile.is_open()) {
		Debug(3) << "Cache file: " << cache_file << " not found.";
		return;
	}
	try {
		json j;
		datafile >> j;

		for (auto& el : j.items()) {
			exif_db_data item = el.value().get<exif_db_data>();
			data.insert_or_assign(el.key(), item);
			// Debug(5) << "Read cache item: " << filename << " - " << item.size << ' ' << item.sharp << ' ' << item.quality << ' ' << item.timestamp;
		}
	}
	catch (...) {
		Debug(1) << "Cache file: " << cache_file << " corrupted.";
		std::exit(EXIT_FAILURE);
	}
}

exif_db::~exif_db() {
	if (dirty && !data.empty()) {
		std::ofstream datafile(cache_file);
		json j(data);
		datafile << j;
	}
}

bool exif_db::get_exif(const std::string& filename, exif_db_data* obj) {
	omp_set_lock(&lock);
	if (data.count(filename)) {
		*obj = data.at(filename);
		omp_unset_lock(&lock);
		Debug(5) << "Cache exif hit: " << filename;
		return 1;
	}
	omp_unset_lock(&lock);
	Debug(5) << "Cache exif miss: " << filename;
	return 0;
}

void exif_db::insert_or_assign(const std::string& filename, const exif_db_data& obj) {
	dirty = 1;
	omp_set_lock(&lock);
	data.insert_or_assign(filename, obj);
	omp_unset_lock(&lock);
}
