#define _LIBCPP_ENABLE_CXX17_REMOVED_AUTO_PTR

#include <Magick++.h>

#include "settings.h"
#include "debug.h"

#ifdef graphicsmagick
void magick_init() {
	Magick::InitializeMagick(nullptr);
}
#endif

int resize(std::string ifile, std::string ofile, ImageSize size, int quality, Unsharp* sharp, bool thumb) {
	Magick::Image image;

	Debug(4) << "Resizing image " << ifile << " to " << ofile << ", size " << size;

	try {
		image.read(ifile);
		if (thumb)
			image.thumbnail(Magick::Geometry(size.width, size.height));
		else
			image.resize(Magick::Geometry(size.width, size.height));
		image.unsharpmask(sharp->radius, sharp->sigma, sharp->amount, sharp->threshold);
		image.quality(quality);
		image.write(ofile);
	}
	catch (Magick::Exception& e) {
		Warn() << "Caught Magick exception: " << e.what();
		return 1;
	}
	return 0;
}
