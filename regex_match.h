#pragma once

#include <string>
#include <filesystem>
#include <regex>

#include "settings.h"

struct cache_data;

// regexes for parsing album.dat file
const std::regex regex_comment("^#(.*)");
const std::regex regex_command("^[A-Z0-9_]+:.*");
const std::regex regex_title("^TITLE:\\s+(.+)");
const std::regex regex_date("^DATE:\\s+(.+)");
const std::regex regex_date_format("^DATE_FORMAT:\\s+(.+)");
const std::regex regex_options("^OPTIONS:\\s+(.+)");
	const std::regex regex_opt_noexif(".*noexif.*");
	const std::regex regex_opt_exif(".*exif.*");
	const std::regex regex_opt_noconv(".*noconv.*");
	const std::regex regex_opt_conv(".*conv.*");
	const std::regex regex_opt_hidden(".*hidden.*");
	const std::regex regex_opt_leaf(".*leaf.*");
	const std::regex regex_opt_count_dir(".*count_dir.*");
	const std::regex regex_opt_count(".*count(?!_dir).*");
const std::regex regex_lens("^LENS:\\s+(\\d+)-(\\d+)\\s+(\\S.*)");
const std::regex regex_band("^BAND:\\s+(\\S+)\\s+(\\S+)\\s+(\\S.*)");
const std::regex regex_break("^BREAK:\\s?(.*)");
const std::regex regex_break_start("^BREAK_START:\\s?(.*)");
const std::regex regex_break_end("^BREAK_END:");
// const std::regex regex_about("^ABOUT:\\s+(.+)\\s*");
const std::regex regex_theme("^THEME:\\s+(.+)\\s*");
const std::regex regex_charset("^CHARSET:\\s+(.+)\\s*");
const std::regex regex_lcss("^LOCAL_CSS:\\s+(.+)\\s*");
const std::regex regex_css("^CSS:\\s+(.+)\\s*");
const std::regex regex_highlight("^HIGHLIGHT:\\s+(.+)\\s*");
const std::regex regex_rss_base("^RSS_BASE:\\s+(.+)\\s*");
const std::regex regex_lcols("^LOCAL_COLUMNS:\\s+([123456789])\\s*");
const std::regex regex_cols("^COLUMNS:\\s+([123456789])\\s*");
// const std::regex regex_table_width("^TABLE_WIDTH:\\s+(\\d*%?)\\s*");
const std::regex regex_limsize("^LOCAL_IMAGE_SIZE:\\s+(\\d*)x(\\d*)\\s*");
const std::regex regex_imsize("^IMAGE_SIZE:\\s+(\\d*)x(\\d*)\\s*");
const std::regex regex_lalsize("^LOCAL_ALBUM_SIZE:\\s+(\\d*)x(\\d*)\\s*");
const std::regex regex_alsize("^ALBUM_SIZE:\\s+(\\d*)x(\\d*)\\s*");
const std::regex regex_lthsize("^LOCAL_THUMB_SIZE:\\s+(\\d*)x(\\d*)\\s*");
const std::regex regex_thsize("^THUMB_SIZE:\\s+(\\d*)x(\\d*)\\s*");
const std::regex regex_thformat("^THUMB_FORMAT:\\s+(.*)");
const std::regex regex_lmeta("^LOCAL_META_KEYWORDS:\\s+(\\S.*)\\s*");
const std::regex regex_meta("^META_KEYWORDS:\\s+(\\S.*)\\s*");
const std::regex regex_footer("^FOOTER:\\s+(.*?)\\s*");
const std::regex regex_header("^HEADER:\\s+(.+)");
const std::regex regex_istats("^ISTATS:\\s+(.+)");
const std::regex regex_more("^MORE:\\s+(.+);(.+)");
// const std::regex regex_gamma("^GAMMA:\\s+(\\S.*)\\s*");
const std::regex regex_unsharp("^UNSHARP:\\s+(\\d+\\.?\\d*)x(\\d+\\.?\\d*)\\+(\\d+\\.?\\d*)\\+(\\d+\\.?\\d*)\\s*");
const std::regex regex_imq("^IMAGE_QUALITY:\\s+(\\S.*)\\s*");
const std::regex regex_thq("^THUMB_QUALITY:\\s+(\\S.*)\\s*");
const std::regex regex_glob("^(r*)\\+(.+)\\s*");
// const std::regex regex_single("^\\s*\\S.*");
const std::regex regex_link("^(!)?@([^;]*);?(.*?)$");
const std::regex regex_single("^(!)?(.*)\\S*");
const std::regex regex_debug("^DEBUG:\\s+(\\d+)");

bool match(const std::regex& rg, const std::string& line, const std::string& desc = "", std::string* var = 0);
bool match(const std::regex& rg, const std::string& line, const std::string& desc, std::filesystem::path* var);
bool match(const std::regex& rg, const std::string& line, const std::string& desc, int* var);
bool match(const std::regex& rg, const std::string& line, const std::string& desc, ImageSize* var);
bool match(const std::regex& rg, const std::string& line, const std::string& desc, Unsharp* var);
bool match_more(const std::string& line, Settings* settings);
bool match_lens(const std::string& line, Settings* settings);
bool match_band(const std::string& line, Settings* settings);
bool match_glob(const std::string& line, std::string* glob, bool* reverse);
bool match_link(const std::string& line, std::string* text, std::string* url, bool* highlight);
bool match_entry(const std::string& line, std::string* file, bool* highlight);

bool match(const std::string& line, std::string* filename, cache_data* data);
